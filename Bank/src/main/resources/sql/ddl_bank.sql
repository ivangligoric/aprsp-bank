DROP TABLE IF EXISTS public.bank CASCADE;
DROP TABLE IF EXISTS public.currency CASCADE;
DROP TABLE IF EXISTS public.account_type CASCADE;
DROP TABLE IF EXISTS public.custom_account_name CASCADE;

create table public.bank(
	id serial primary key,
	bank_name varchar(128) not null,
	website varchar(255) not null
);

CREATE table public.currency(
	id serial primary key,
	currency_code varchar(3),
	currency_name varchar(128)
);

create table public.account_type(
	id serial primary key,
	type_name varchar(128) not null,
	currency_id int not null,
	
	constraint fk_currency foreign key (currency_id) references currency(id)
);

create table public.bank_account(
id serial primary key,
account_number varchar(128) not null,
balance decimal not null,
customer_id int not null,
bank_id int not null,
bank_account_type int not null,
	
constraint fk_bank_id foreign key(bank_id) references bank(id),
constraint fk_bank_type foreign key(bank_account_type) references account_type(id),
constraint uq_bank_account_number unique(account_number)
);

create table custom_account_name(
id serial primary key,
custom_account_name varchar(128),
bank_account_creator int references public.bank_account(id),
bank_account_mapped int references public.bank_account(id)
 
);


create index pk_bank_id on public.bank(id);

create index pk_currency_id on public.currency(id);

create index pk_account_type_id on public.account_type(id);

create index pk_cust_acc_name on public.custom_account_name(id);

create index pk_bank_account on public.bank_account(id);