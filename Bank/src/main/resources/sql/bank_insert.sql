insert into public.currency (currency_code, currency_name)
values('RSD', 'Dinar');

insert into public.currency (currency_code, currency_name)
values('EUR', 'Euro');

insert into public.currency (currency_code, currency_name)
values('USD', 'Američki dolar');

insert into public.currency (currency_code, currency_name)
values('CHF', 'Švajcarski franak');


insert into public.account_type( type_name, currency_id)
	values ( 'Dinarski', 1);
	
insert into public.account_type( type_name, currency_id)
	values ( 'Devizni euro', 2);
	
insert into public.account_type( type_name, currency_id)
	values ( 'Devizni dolar', 3);
	
insert into public.account_type( type_name, currency_id)
values ( 'Devizni franak', 4);


insert into public.bank(bank_name, website)
	values ('Banca Intesa', 'www.bancaintesa.com');

insert into public.bank(bank_name, website)
	values ('Komercijalna banka', 'www.komercijalnabanka.com');

insert into public.bank(bank_name, website)
	values ('ERSTE banka', 'www.erstebanka.com');
	
	
insert into public.bank_account(account_number, balance, customer_id, bank_id, bank_account_type)
	values ('1234567', 60000, 1, 1, 1);
	
insert into public.bank_account(account_number, balance, customer_id, bank_id, bank_account_type)
	values ('2222222', 1000000, 1, 1, 2);
	
insert into public.bank_account(account_number, balance, customer_id, bank_id, bank_account_type)
	values ('3333333', 40000, 2, 2, 1);
	
insert into public.bank_account(account_number, balance, customer_id, bank_id, bank_account_type)
	values ('4444444', 95000, 3, 3, 1);
	
insert into public.bank_account(account_number, balance, customer_id, bank_id, bank_account_type)
	values ('5555555', 60000, 4, 3, 2);
	
	
insert into  public.custom_account_name (custom_account_name, bank_account_creator, bank_account_mapped)
	values ('Telenor', 8, 7);
	
insert into  public.custom_account_name (custom_account_name, bank_account_creator, bank_account_mapped)
	values ('Zaposleni', 7, 8);