package aprsp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import aprsp.model.Currency;

public interface CurrencyRepository extends JpaRepository<Currency, Integer>{

	@Query(value = "select nextval('currency_id_seq')", nativeQuery = true)
	Integer getGeneratedId();

}
