package aprsp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import aprsp.model.Bank;

public interface BankRepository extends JpaRepository<Bank, Integer> {

	@Query(value = "select nextval('bank_id_seq')", nativeQuery = true)
	Integer getGeneratedId();

}
