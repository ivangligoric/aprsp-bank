package aprsp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import aprsp.model.AccountType;

public interface AccountTypeRepository extends JpaRepository<AccountType, Integer>{

	@Query(value = "select nextval('account_type_id_seq')", nativeQuery = true)
	Integer getGeneratedId();

}
