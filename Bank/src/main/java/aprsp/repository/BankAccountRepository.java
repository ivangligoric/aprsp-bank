package aprsp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import aprsp.model.BankAccount;

public interface BankAccountRepository extends JpaRepository<BankAccount, Integer>{

	BankAccount findByAccountNumber(String accountNumber);
	
	@Query(value = "select nextval('bank_account_id_seq')", nativeQuery = true)
	Integer getGeneratedId();
	
	BankAccount findByCustomerId(Integer customerId);

}
