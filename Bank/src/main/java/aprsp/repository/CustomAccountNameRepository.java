package aprsp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import aprsp.model.BankAccount;
import aprsp.model.CustomAccountName;

public interface CustomAccountNameRepository extends JpaRepository<CustomAccountName, Integer>{

	@Query(value = "select nextval('custom_account_name_id_seq')", nativeQuery = true)
	Integer getGeneratedId();

	List<CustomAccountName> findByBankAccountCreator(BankAccount bankAccountCreator);
}
