package aprsp.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the bank_account database table.
 * 
 */
@Entity
@Table(name="bank_account")
@NamedQuery(name="BankAccount.findAll", query="SELECT b FROM BankAccount b")
public class BankAccount implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="BANK_ACCOUNT_ID_GENERATOR", sequenceName="bank_account_id_seq")
    @Column(name = "id", updatable = false)
	private Integer id;

	@Column(name="account_number")
	private String accountNumber;

	@Column(name="balance")
	private BigDecimal balance;

	@Column(name="customer_id", updatable = false)
	private Integer customerId;

	//bi-directional many-to-one association to AccountType
	@ManyToOne
	@JoinColumn(name="bank_account_type")
	private AccountType accountType;

	//bi-directional many-to-one association to Bank
	@ManyToOne
	private Bank bank;

	//bi-directional many-to-one association to CustomAccountName1
	@OneToMany(mappedBy="bankAccountCreator")
	@JsonIgnore
	private List<CustomAccountName> customAccountNamesCreator;

	//bi-directional many-to-one association to CustomAccountName1
	@OneToMany(mappedBy="bankAccountMapped")
	@JsonIgnore
	private List<CustomAccountName> customAccountNamesMapped;

	public BankAccount() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAccountNumber() {
		return this.accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public BigDecimal getBalance() {
		return this.balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public Integer getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public AccountType getAccountType() {
		return this.accountType;
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}

	public Bank getBank() {
		return this.bank;
	}

	public void setBank(Bank bank) {
		this.bank = bank;
	}

	public List<CustomAccountName> getCustomAccountNamesCreator() {
		return this.customAccountNamesCreator;
	}

	public void setCustomAccountNamesCreator(List<CustomAccountName> customAccountNamesCreator) {
		this.customAccountNamesCreator = customAccountNamesCreator;
	}

	public CustomAccountName addCustomAccountNamesCreator(CustomAccountName customAccountNamesCreator) {
		getCustomAccountNamesCreator().add(customAccountNamesCreator);
		customAccountNamesCreator.setBankAccountCreator(this);

		return customAccountNamesCreator;
	}

	public CustomAccountName removeCustomAccountNamesCreator(CustomAccountName customAccountNamesCreator) {
		getCustomAccountNamesCreator().remove(customAccountNamesCreator);
		customAccountNamesCreator.setBankAccountCreator(null);

		return customAccountNamesCreator;
	}

	public List<CustomAccountName> getCustomAccountNamesMapped() {
		return this.customAccountNamesMapped;
	}

	public void setCustomAccountNamesMapped(List<CustomAccountName> customAccountNamesMapped) {
		this.customAccountNamesMapped = customAccountNamesMapped;
	}

	public CustomAccountName addCustomAccountNamesMapped(CustomAccountName customAccountNamesMapped) {
		getCustomAccountNamesMapped().add(customAccountNamesMapped);
		customAccountNamesMapped.setBankAccountMapped(this);

		return customAccountNamesMapped;
	}

	public CustomAccountName removeCustomAccountNamesMapped(CustomAccountName customAccountNamesMapped) {
		getCustomAccountNamesMapped().remove(customAccountNamesMapped);
		customAccountNamesMapped.setBankAccountMapped(null);

		return customAccountNamesMapped;
	}

}