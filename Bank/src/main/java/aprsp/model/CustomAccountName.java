package aprsp.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the custom_account_name database table.
 * 
 */
@Entity
@Table(name="custom_account_name")
@NamedQuery(name="CustomAccountName.findAll", query="SELECT c FROM CustomAccountName c")
public class CustomAccountName implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="CUSTOM_ACCOUNT_NAME_ID_GENERATOR", sequenceName="custom_account_name_id_seq")
    @Column(name = "id", updatable = false)
	private Integer id;

	@Column(name="custom_account_name")
	private String customAccountName;

	//bi-directional many-to-one association to BankAccount
	@ManyToOne
	@JoinColumn(name="bank_account_creator")
	private BankAccount bankAccountCreator;

	//bi-directional many-to-one association to BankAccount
	@ManyToOne
	@JoinColumn(name="bank_account_mapped")
	private BankAccount bankAccountMapped;

	public CustomAccountName() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCustomAccountName() {
		return this.customAccountName;
	}

	public void setCustomAccountName(String customAccountName) {
		this.customAccountName = customAccountName;
	}

	public BankAccount getBankAccountCreator() {
		return this.bankAccountCreator;
	}

	public void setBankAccountCreator(BankAccount bankAccountCreator) {
		this.bankAccountCreator = bankAccountCreator;
	}

	public BankAccount getBankAccountMapped() {
		return this.bankAccountMapped;
	}

	public void setBankAccountMapped(BankAccount bankAccountMapped) {
		this.bankAccountMapped = bankAccountMapped;
	}

}