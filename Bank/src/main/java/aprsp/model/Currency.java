package aprsp.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;


/**
 * The persistent class for the currency database table.
 * 
 */
@Entity
@NamedQuery(name="Currency.findAll", query="SELECT c FROM Currency c")
public class Currency implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="CURRENCY_ID_GENERATOR", sequenceName="currency_id_seq")
    @Column(name = "id", updatable = false)
	private Integer id;

	@Column(name="currency_code")
	private String currencyCode;

	@Column(name="currency_name")
	private String currencyName;

	//bi-directional many-to-one association to AccountType
	@OneToMany(mappedBy="currency")
	@JsonIgnore
	private List<AccountType> accountTypes;

	public Currency() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getCurrencyName() {
		return this.currencyName;
	}

	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}

	public List<AccountType> getAccountTypes() {
		return this.accountTypes;
	}

	public void setAccountTypes(List<AccountType> accountTypes) {
		this.accountTypes = accountTypes;
	}

	public AccountType addAccountType(AccountType accountType) {
		getAccountTypes().add(accountType);
		accountType.setCurrency(this);

		return accountType;
	}

	public AccountType removeAccountType(AccountType accountType) {
		getAccountTypes().remove(accountType);
		accountType.setCurrency(null);

		return accountType;
	}

}