package aprsp.domain;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.PrePersist;

public class Transaction {

	private Integer id;

	private BigDecimal amount;

	private String bankAccountReceiver;

	private String bankAccountSender;

	private Date dateCreated;

	private String purpose;

	public Transaction() {
	}
	
	@PrePersist
	protected void onCreate() {
	    dateCreated = new Date();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getBankAccountReceiver() {
		return this.bankAccountReceiver;
	}

	public void setBankAccountReceiver(String bankAccountReceiver) {
		this.bankAccountReceiver = bankAccountReceiver;
	}

	public String getBankAccountSender() {
		return this.bankAccountSender;
	}

	public void setBankAccountSender(String bankAccountSender) {
		this.bankAccountSender = bankAccountSender;
	}

	public Date getDateCreated() {
		return this.dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getPurpose() {
		return this.purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}
}
