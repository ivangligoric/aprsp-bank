package aprsp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import aprsp.model.Currency;
import aprsp.service.CurrencyService;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api")
public class CurrencyController {

	private CurrencyService currencyService;
	
	@Autowired
	public CurrencyController(CurrencyService currencyService) {
		// TODO Auto-generated constructor stub
		this.currencyService = currencyService;
	}
	
	@ApiOperation("Returns all currencies")
	@GetMapping("/currency")
	public List<Currency> getAll(){
		return currencyService.findAll();
	}
	
	@ApiOperation("Returns currency by id")
	@GetMapping("/currency/{id}")
	public ResponseEntity<Currency> getOne(@PathVariable("id") Integer id){
		Currency currency = currencyService.findOne(id);
		if(currency != null)
			return new ResponseEntity<Currency>(currency, HttpStatus.OK);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@ApiOperation("Insert new currency in database")
	@PostMapping("/currency")
	public ResponseEntity<Currency> insert(@RequestBody Currency currency){
		currencyService.save(currency);
		return new ResponseEntity<Currency>(currency, HttpStatus.CREATED);
	}
	
	@ApiOperation("Update currency")
	@PutMapping("/currency/{id}")
	public ResponseEntity<Currency> update(@RequestBody Currency currency, @PathVariable("id") Integer id){
		if(currencyService.findOne(id) == null)
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		currency.setId(id);
		currencyService.save(currency);
		return new ResponseEntity<Currency>(currencyService.findOne(id), HttpStatus.OK);
	}
	
	@ApiOperation("Delete currency by id")
	@DeleteMapping("/currency/{id}")
	public ResponseEntity<Void> delete(@PathVariable("id") Integer id){
		if(currencyService.findOne(id) == null)
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		currencyService.delete(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
