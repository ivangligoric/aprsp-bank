package aprsp.controller;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import aprsp.model.CustomAccountName;
import aprsp.domain.Transaction;
import aprsp.service.CustomAccountNameServiceImpl;

@RestController
@RequestMapping("/api")
public class TransactionController {

	private CustomAccountNameServiceImpl customAccountNameServiceImpl;
	
	@Autowired
	public TransactionController(CustomAccountNameServiceImpl customAccountNameServiceImpl) {
		// TODO Auto-generated constructor stub
		this.customAccountNameServiceImpl = customAccountNameServiceImpl;
	}
	
	@PostMapping("/transferAmount")
	public ResponseEntity<Transaction> transferAmmount(@RequestBody Transaction transaction){
		customAccountNameServiceImpl.transferAmmount(transaction);
		return new ResponseEntity<Transaction>(transaction, HttpStatus.OK);
	}
	
	@PostMapping("/transferAmountByName/{amount}")
	public ResponseEntity<Transaction> transferAmountByName(@RequestBody CustomAccountName customAccountName, @PathVariable("amount") BigDecimal amount){
		Transaction transaction = customAccountNameServiceImpl.transferAmountByAccountName(customAccountName, amount);
		if(transaction != null)
			return new ResponseEntity<>(transaction, HttpStatus.OK);
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
}
