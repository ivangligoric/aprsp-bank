package aprsp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import aprsp.model.CustomAccountName;
import aprsp.service.CustomAccountNameService;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api")
public class CustomAccountNameController {

	private CustomAccountNameService customAccountNameService;
	
	@Autowired
	public CustomAccountNameController(CustomAccountNameService customAccountNameService) {
		// TODO Auto-generated constructor stub
		this.customAccountNameService = customAccountNameService;
	}
	
	@ApiOperation("Returns all custom account names")
	@GetMapping("/customAccountName")
	public List<CustomAccountName> getAll(){
		return customAccountNameService.findAll();
	}
	
	@ApiOperation("Returns accounts by customer account name")
	@GetMapping("/accounts/{account}")
	public List<CustomAccountName> getByBankCreator(@PathVariable("account")String account){
		return customAccountNameService.findByBankAccountCreator(account);
	}
	
	@ApiOperation("Returns custom account name by id")
	@GetMapping("/customAccountName/{id}")
	public ResponseEntity<CustomAccountName> getOne(@PathVariable("id") Integer id){
		CustomAccountName customAccountName = customAccountNameService.findOne(id);
		if(customAccountName != null)
			return new ResponseEntity<CustomAccountName>(customAccountName, HttpStatus.OK);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@ApiOperation("Insert new custom account name in database")
	@PostMapping("/customAccountName")
	public ResponseEntity<CustomAccountName> insert(@RequestBody CustomAccountName customAccountName){
		customAccountNameService.save(customAccountName);
		return new ResponseEntity<CustomAccountName>(customAccountName, HttpStatus.CREATED);
	}
	
	@ApiOperation("Update custom account name")
	@PutMapping("/customAccountName/{id}")
	public ResponseEntity<CustomAccountName> update(@RequestBody CustomAccountName customAccountName, @PathVariable("id") Integer id){
		if(customAccountNameService.findOne(id) == null)
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		customAccountName.setId(id);
		customAccountNameService.save(customAccountName);
		return new ResponseEntity<CustomAccountName>(customAccountName, HttpStatus.OK);
	}
	
	@ApiOperation("Delete custom account name by id")
	@DeleteMapping("/customAccountName/{id}")
	public ResponseEntity<Void> delete(@PathVariable("id") Integer id){
		if(customAccountNameService.findOne(id) == null)
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		customAccountNameService.delete(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
