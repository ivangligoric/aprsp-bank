package aprsp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import aprsp.model.BankAccount;
import aprsp.service.BankAccountService;
import aprsp.service.CustomerService;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api")
public class BankAccountController {

	private BankAccountService bankAccountService;
	
	private CustomerService customerService;
	
	@Autowired
	public BankAccountController(BankAccountService bankAccountService, CustomerService customerService) {
		// TODO Auto-generated constructor stub
		this.bankAccountService = bankAccountService;
		this.customerService = customerService;
	}
	
	@ApiOperation("Returns all bank accounts")
	@GetMapping("/bankAccount")
	public List<BankAccount> getAll(){
		return bankAccountService.findAll();
	}
	
	@ApiOperation("Return bank account by id")
	@GetMapping("/bankAccount/{id}")
	public ResponseEntity<BankAccount> getOne(@PathVariable("id") Integer id){
		BankAccount bankAccount = bankAccountService.findOne(id);
		if(bankAccount != null)
			return new ResponseEntity<BankAccount>(bankAccount, HttpStatus.OK);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@ApiOperation("Insert new bank account in database")
	@PostMapping("/bankAccount")
	public ResponseEntity<BankAccount> insert(@RequestBody BankAccount bankAccount){
		bankAccountService.save(bankAccount);
		return new ResponseEntity<BankAccount>(bankAccount, HttpStatus.CREATED);
	}
	
	@ApiOperation("Update bank account")
	@PutMapping("/bankAccount/{id}")
	public ResponseEntity<BankAccount> update(@RequestBody BankAccount bankAccount, @PathVariable("id") Integer id){
		if(bankAccountService.findOne(id) == null)
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		bankAccount.setId(id);
		bankAccountService.save(bankAccount);
		return new ResponseEntity<BankAccount>(bankAccount, HttpStatus.OK);
	}
	
	@DeleteMapping("/bankAccount/{id}")
	public ResponseEntity<Void> delete(@PathVariable("id") Integer id){
		if(bankAccountService.findOne(id) == null)
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		bankAccountService.delete(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@ApiOperation("Delete bank account")
	@DeleteMapping("/deleteCustomer/{id}")
	public ResponseEntity<Void> customerExists(@PathVariable("id") Integer id) {
		boolean deleted = customerService.deletedByCustomerId(id);
		if(deleted)
			return new ResponseEntity<>(HttpStatus.OK);
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
}
