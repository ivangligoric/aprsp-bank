package aprsp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import aprsp.model.AccountType;
import aprsp.service.AccountTypeService;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api")
public class AccountTypeController {

	private AccountTypeService accountTypeService;
	
	@Autowired
	public AccountTypeController(AccountTypeService accountTypeService) {
		// TODO Auto-generated constructor stub
		this.accountTypeService = accountTypeService;
	}
	
	@ApiOperation("Returns all account types")
	@GetMapping("/accountType")
	public List<AccountType> findAll(){
		return accountTypeService.findAll();
	}
	
	@ApiOperation("Returns account type by id")
	@GetMapping("/accountType/{id}")
	public ResponseEntity<AccountType> getOne(@PathVariable("id") Integer id){
		AccountType accountType = accountTypeService.findOne(id);
		if(accountType != null)
			return new ResponseEntity<AccountType>(accountType, HttpStatus.OK);
		return new ResponseEntity<AccountType>(HttpStatus.NO_CONTENT);
	}
	
	@ApiOperation("Insert new account type")
	@PostMapping("/accountType")
	public ResponseEntity<AccountType> insert(@RequestBody AccountType accountType){
		accountTypeService.save(accountType);
		return new ResponseEntity<AccountType>(accountType, HttpStatus.CREATED);
	}
	
	@ApiOperation("Update account type")
	@PutMapping("/accountType/{id}")
	public ResponseEntity<AccountType> update(@RequestBody AccountType accountType, @PathVariable("id") Integer id){
		if(accountTypeService.findOne(id) == null)
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		accountType.setId(id);
		accountTypeService.save(accountType);
		return new ResponseEntity<AccountType>(accountTypeService.findOne(id), HttpStatus.OK);
	}
	
	@ApiOperation("Delete account type by id")
	@DeleteMapping("/accountType/{id}")
	public ResponseEntity<Void> delete(@PathVariable("id") Integer id){
		if(accountTypeService.findOne(id) == null)
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		accountTypeService.delete(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
