package aprsp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import aprsp.model.Bank;
import aprsp.service.BankService;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api")
public class BankController {

	private BankService bankService;
	
	@Autowired
	public BankController(BankService bankService) {
		// TODO Auto-generated constructor stub
		this.bankService = bankService;
	}
	
	@ApiOperation("Returns all banks")
	@GetMapping("/bank")
	public List<Bank> getAll(){
		return bankService.findAll();
	}
	
	@ApiOperation("Returns bank by id")
	@GetMapping("/bank/{id}")
	public ResponseEntity<Bank> getOne(@PathVariable("id") Integer id) {
		Bank bank = bankService.findOne(id);
		if(bank != null)
			return new ResponseEntity<Bank>(bank, HttpStatus.OK);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@ApiOperation("Insert new bank in database")
	@PostMapping("/bank")
	public ResponseEntity<Bank> insert(@RequestBody Bank bank){
		bankService.save(bank);
		return new ResponseEntity<>(bank, HttpStatus.CREATED);
	}
	
	@ApiOperation("Update bank")
	@PutMapping("/bank/{id}")
	public ResponseEntity<Bank> update(@RequestBody Bank bank, @PathVariable("id") Integer id){
		if(bankService.findOne(id) == null)
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		bank.setId(id);
		bankService.save(bank);
		return new ResponseEntity<Bank>(bankService.findOne(id), HttpStatus.OK);
	}
	
	@ApiOperation("Delete bank by id")
	@DeleteMapping("/bank/{id}")
	public ResponseEntity<Void> delete(@PathVariable("id") Integer id){
		if(bankService.findOne(id) == null)
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		bankService.delete(id);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

}
