package aprsp.service;

import java.util.List;

import org.springframework.stereotype.Component;

import aprsp.model.AccountType;

@Component
public interface AccountTypeService {

	List<AccountType> findAll();
	
	AccountType findOne(Integer id);
	
	AccountType save(AccountType accountType);
	
	void delete(Integer id);
}
