package aprsp.service;

import org.springframework.stereotype.Component;

@Component
public interface CustomerService {

	boolean deletedByCustomerId(Integer CustomerId);
}
