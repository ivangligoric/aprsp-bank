package aprsp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import aprsp.domain.Customer;
import aprsp.model.BankAccount;
import aprsp.repository.BankAccountRepository;

@Service
public class BankAccountServiceImpl implements BankAccountService{

	private BankAccountRepository bankAccountRepository;
	
	private RestTemplate restTemplate;
	
	@Autowired
	public BankAccountServiceImpl(BankAccountRepository bankAccountRepository, RestTemplate restTemplate) {
		// TODO Auto-generated constructor stub
		this.bankAccountRepository = bankAccountRepository;
		this.restTemplate = restTemplate;
	}
	@Override
	public List<BankAccount> findAll() {
		// TODO Auto-generated method stub
		return bankAccountRepository.findAll();
	}

	@Override
	public BankAccount findOne(Integer id) {
		// TODO Auto-generated method stub
		return bankAccountRepository.findById(id).orElse(null);
	}

	@Override
	public BankAccount save(BankAccount bankAccount) {
		// TODO Auto-generated method stub
		if(bankAccount.getId() == null) {
			bankAccount.setId(bankAccountRepository.getGeneratedId());
			restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
	        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
			String uri = "http://localhost:9100/api/customer/"+ bankAccount.getCustomerId();
			Customer responseEntity = restTemplate.getForObject(uri, Customer.class);
			if(responseEntity == null || responseEntity.getId() == null)
				return null;
			bankAccount.setCustomerId(responseEntity.getId());
		}
		bankAccountRepository.save(bankAccount);
		return bankAccount;
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		bankAccountRepository.deleteById(id);
		
	}
}
