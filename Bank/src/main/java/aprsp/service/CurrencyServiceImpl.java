package aprsp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aprsp.model.Currency;
import aprsp.repository.CurrencyRepository;

@Service
public class CurrencyServiceImpl implements CurrencyService{

	private CurrencyRepository currencyRepository;
	
	@Autowired
	public CurrencyServiceImpl(CurrencyRepository currencyRepository) {
		// TODO Auto-generated constructor stub
		this.currencyRepository = currencyRepository;
	}
	
	@Override
	public List<Currency> findAll() {
		// TODO Auto-generated method stub
		return currencyRepository.findAll();
	}

	@Override
	public Currency findOne(Integer id) {
		// TODO Auto-generated method stub
		return currencyRepository.findById(id).orElse(null);
	}

	@Override
	public Currency save(Currency currency) {
		// TODO Auto-generated method stub
		if(currency.getId() == null)
			currency.setId(currencyRepository.getGeneratedId());
		currencyRepository.save(currency);
		return currency;
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		currencyRepository.deleteById(id);
		
	}

}
