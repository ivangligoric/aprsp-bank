package aprsp.service;

import java.util.List;

import org.springframework.stereotype.Component;

import aprsp.model.BankAccount;

@Component
public interface BankAccountService {

	List<BankAccount> findAll();
	
	BankAccount findOne(Integer id);
	
	BankAccount save(BankAccount bankAccount);
	
	void delete(Integer id);
	
}
