package aprsp.service;

import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import aprsp.repository.BankAccountRepository;

@Service
public class CustomerServiceImpl implements CustomerService{

	private BankAccountRepository bankAccountRepository;
	
	private RestTemplate restTemplate;
	
	public CustomerServiceImpl(BankAccountRepository bankAccountRepository, RestTemplate restTemplate) {
		// TODO Auto-generated constructor stub
		this.bankAccountRepository = bankAccountRepository;
		this.restTemplate = restTemplate;
	}
	
	@Override
	public boolean deletedByCustomerId(Integer customerId) {
		// TODO Auto-generated method stub
		if(bankAccountRepository.findByCustomerId(customerId) == null) {
			restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
	        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
			String uri = "http://localhost:9100/api/customer/"+customerId;
			restTemplate.delete(uri);
			return true;
		}
		return false;
	}

	
}
