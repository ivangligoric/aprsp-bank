package aprsp.service;

import java.util.List;

import org.springframework.stereotype.Component;

import aprsp.model.Bank;

@Component
public interface BankService {

	List<Bank> findAll();
	
	Bank findOne(Integer id);
	
	Bank save(Bank bank);
	
	void delete(Integer id);
}
