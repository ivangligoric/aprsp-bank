package aprsp.service;

import java.math.BigDecimal;

import org.springframework.stereotype.Component;

import aprsp.model.CustomAccountName;
import aprsp.domain.Transaction;

@Component
public interface TransactionService {

	Transaction transferAmmount(Transaction transaction);
	
	Transaction transferAmountByAccountName(CustomAccountName customAccountName, BigDecimal amount);
}
