package aprsp.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import aprsp.model.BankAccount;
import aprsp.model.CustomAccountName;
import aprsp.domain.Transaction;
import aprsp.repository.BankAccountRepository;
import aprsp.repository.CustomAccountNameRepository;

@Service
public class CustomAccountNameServiceImpl implements CustomAccountNameService, TransactionService{

	private CustomAccountNameRepository customAccountNameRepository;
	
	private BankAccountRepository bankAccountRepository;
	
	private RestTemplate restTemplate;
	
	@Autowired
	public CustomAccountNameServiceImpl(CustomAccountNameRepository customAccountNameRepository, BankAccountRepository bankAccountRepository, RestTemplate restTemplate) {
		// TODO Auto-generated constructor stub
		this.customAccountNameRepository = customAccountNameRepository;
		this.bankAccountRepository = bankAccountRepository;
		this.restTemplate = restTemplate;
	}
	
	@Override
	public List<CustomAccountName> findAll() {
		// TODO Auto-generated method stub
		return customAccountNameRepository.findAll();
	}

	@Override
	public CustomAccountName findOne(Integer id) {
		// TODO Auto-generated method stub
		return customAccountNameRepository.findById(id).orElse(null);
	}
	
	@Override
	public List<CustomAccountName> findByBankAccountCreator(String bankAccountCreator) {
		// TODO Auto-generated method stub
		BankAccount bankAccount = bankAccountRepository.findByAccountNumber(bankAccountCreator);
		if(bankAccount == null)
			return null;
		List<CustomAccountName> accountNames = customAccountNameRepository.findByBankAccountCreator(bankAccount);
		return accountNames;
		
	}

	@Override
	public CustomAccountName save(CustomAccountName customAccountName) {
		// TODO Auto-generated method stub
		if(customAccountName.getId() == null)
			customAccountName.setId(customAccountNameRepository.getGeneratedId());
		if(!customAccountName.getBankAccountCreator().equals(customAccountName.getBankAccountMapped())
				&& customAccountName.getBankAccountCreator().getAccountType().getId()
				.equals(customAccountName.getBankAccountMapped().getAccountType().getId())) {
			customAccountNameRepository.save(customAccountName);
			return customAccountName;
		}
		return null;
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		customAccountNameRepository.deleteById(id);
	}

	@Override
	public Transaction transferAmmount(Transaction transaction) {
		// TODO Auto-generated method stub
		BankAccount bankAccountSender = bankAccountRepository.findByAccountNumber(transaction.getBankAccountSender());
		BankAccount bankAccountReceiver = bankAccountRepository.findByAccountNumber(transaction.getBankAccountReceiver());
	
		if(bankAccountSender == null || bankAccountReceiver == null || 
				bankAccountSender.getBalance().compareTo(transaction.getAmount()) < 0
				|| !bankAccountSender.getAccountType().equals(bankAccountReceiver.getAccountType()))
			return null;
		bankAccountSender.setBalance(bankAccountSender.getBalance().subtract(transaction.getAmount()));
		bankAccountReceiver.setBalance(bankAccountReceiver.getBalance().add(transaction.getAmount()));
		bankAccountRepository.save(bankAccountSender);
		bankAccountRepository.save(bankAccountReceiver);
		
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
		String uri = "http://localhost:9300/api/transaction";
		restTemplate.postForObject(uri, transaction, Transaction.class);
		
		return transaction;
	}

	@Override
	public Transaction transferAmountByAccountName(CustomAccountName customAccountName, BigDecimal amount) {
		// TODO Auto-generated method stub
		Transaction transaction = new Transaction();
		transaction.setAmmount(amount);
		transaction.setBankAccountSender(customAccountName.getBankAccountCreator().getAccountNumber());
		transaction.setBankAccountReceiver(customAccountName.getBankAccountMapped().getAccountNumber());
		return this.transferAmmount(transaction);
	}

	

}
