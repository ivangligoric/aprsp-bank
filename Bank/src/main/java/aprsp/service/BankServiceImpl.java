package aprsp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aprsp.model.Bank;
import aprsp.repository.BankRepository;

@Service
public class BankServiceImpl implements BankService{

	private BankRepository bankRepository;
	
	@Autowired
	public BankServiceImpl(BankRepository bankRepository) {
		// TODO Auto-generated constructor stub
		this.bankRepository = bankRepository;
	}
	
	@Override
	public List<Bank> findAll() {
		// TODO Auto-generated method stub
		return bankRepository.findAll();
	}

	@Override
	public Bank findOne(Integer id) {
		// TODO Auto-generated method stub
		return bankRepository.findById(id).orElse(null);
	}

	@Override
	public Bank save(Bank bank) {
		// TODO Auto-generated method stub
		if(bank.getId() == null)
			bank.setId(bankRepository.getGeneratedId());
		System.out.println(bank.getId());
		bankRepository.save(bank);
		return bank;
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		bankRepository.deleteById(id);
	}

}
