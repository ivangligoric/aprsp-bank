package aprsp.service;

import java.util.List;

import org.springframework.stereotype.Component;

import aprsp.model.Currency;

@Component
public interface CurrencyService {

	List<Currency> findAll();
	
	Currency findOne(Integer id);
	
	Currency save(Currency currency);
	
	void delete(Integer id);
}
