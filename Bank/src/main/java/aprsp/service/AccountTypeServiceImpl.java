package aprsp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aprsp.model.AccountType;
import aprsp.repository.AccountTypeRepository;

@Service
public class AccountTypeServiceImpl implements AccountTypeService {

	private AccountTypeRepository accountTypeRepository;
	
	@Autowired
	public AccountTypeServiceImpl(AccountTypeRepository accountTypeRepository) {
		this.accountTypeRepository = accountTypeRepository;
	}
	
	@Override
	public List<AccountType> findAll() {
		// TODO Auto-generated method stub
		return accountTypeRepository.findAll();
	}

	@Override
	public AccountType findOne(Integer id) {
		// TODO Auto-generated method stub
		return accountTypeRepository.findById(id).orElse(null);
	}

	@Override
	public AccountType save(AccountType accountType) {
		// TODO Auto-generated method stub
		if(accountType.getId() == null)
			accountType.setId(accountTypeRepository.getGeneratedId());
		accountTypeRepository.save(accountType);
		return accountType;
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		accountTypeRepository.deleteById(id);
	}

	
}
