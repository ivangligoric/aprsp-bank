package aprsp.service;

import java.util.List;

import org.springframework.stereotype.Component;

import aprsp.model.CustomAccountName;

@Component
public interface CustomAccountNameService {

	List<CustomAccountName> findAll();
	
	CustomAccountName findOne(Integer id);
	
	List<CustomAccountName> findByBankAccountCreator(String bankAccountCreator);
	
	CustomAccountName save(CustomAccountName customAccountName);
	
	void delete(Integer id);
}
